;;;; Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (torrent fibers)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (ice-9 match)
  #:use-module (torrent utils fibers)
  #:use-module (torrent peer)
  #:use-module (torrent protocol)
  #:export (peer-fiber))

(define (peer-read-fiber channel sock)
  (lambda ()
    (catch #t
      (lambda ()
        (let loop ()
          (put-message channel '(("type" . 'received)
                                 ("message" . (receive-message sock))))
          (loop)))
      (lambda () #t))))

(define (peer-fiber command-channel back-channel sock)
  (let ((read-channel (make-channel)))
    (spawn-fiber (peer-read-fiber read-channel sock))
    (commanded-fiber
      (lambda _
        (put-message back-channel '(("type" . "error"))))
      (command-channel read-channel)
      ('received => (lambda* (#:key type message)
                      (match message
                        ('keepalive #t); XXX: should we really not react to a keepalive?
                        (_ (put-message back-channel message)))))
      ('timeout => (lambda* (#:key type)
                     (send-keep-alive sock)))
      ('chocke => (lambda* (#:key type)
                    (send-choke sock)))
      ('unchocke => (lambda* (#:key type)
                      (send-unchoke sock)))
      ('interested => (lambda* (#:key type)
                        (send-interested sock)))
      ('not-interested => (lambda* (#:key type)
                            (send-not-interested sock)))
      ('have => (lambda* (#:key type have)
                  (send-have sock have)))
      ('bitfield => (lambda* (#:key type bitfield)
                      (send-bitfield sock bitfield)))
      ('request => (lambda* (#:key type index start len)
                     (send-request sock index start len)))
      ('piece => (lambda* (#:key type index start block)
                   (send-piece sock index start block)))
      ('cancel => (lambda* (#:key type index start len)
                    (send-cancel sock index start len)))
      ('port => (lambda* (#:key type port)
                  (send-port sock port))))))
