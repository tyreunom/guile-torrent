;;;; Copyright (C) 2019 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (torrent peer)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 match)
  #:use-module (rnrs base)
  #:use-module (rnrs bytevectors)
  #:use-module (torrent utils fibers)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:export (make-peer
            peer?
            peer-id
            peer-sock
            peer-bitfield
            peer-choking
            peer-interested
            peer-am-choking
            peer-am-interested
            peer-channel

            make-potential-peer
            potential-peer?
            potential-peer-host
            potential-peer-port
            potential-peer-freshness

            generate-id
            get-new-peer
            update-peer
            
            merge-potential-peers))

(define-record-type peer
  (make-peer id sock bitfield choking interested am-choking am-interested channel state)
  peer?
  (id            peer-id)
  (sock          peer-sock)
  (bitfield      peer-bitfield)
  (choking       peer-choking)
  (interested    peer-interested)
  (am-choking    peer-am-choking)
  (am-interested peer-am-interested)
  (channel       peer-channel)
  (state         peer-state))

(define-record-type potential-peer
  (make-potential-peer host port freshness)
  potential-peer?
  (host potential-peer-host)
  (port potential-peer-port)
  (freshness potential-peer-freshness))

(define* (update-peer peer
                      #:key
                      (id (peer-id peer)) (sock (peer-sock peer))
                      (bitfield (peer-bitfield peer))
                      (choking (peer-choking peer))
                      (interested (peer-interested peer))
                      (am-choking (peer-am-choking peer))
                      (am-interested (peer-am-interested peer))
                      (channel (peer-channel peer))
		      (state (peer-state peer)))
  (make-peer id sock bitfield choking interested am-choking am-interested channel state))

(define* (get-new-peer host port #:optional id)
  (let* ((info (car (getaddrinfo host (number->string port))))
         (sock (socket (addrinfo:fam info) SOCK_STREAM IPPROTO_IP)))
    (let ((flags (fcntl sock F_GETFL)))
      (fcntl sock F_SETFL (logior O_NONBLOCK flags)))
    (connect sock (addrinfo:addr info))
    (make-peer id sock #f #t #f #t #f (make-channel) 'idle)))

(define (random-alphanum-char)
  (let ((r (random 127)))
    (if (char-set-contains? char-set:letter+digit (integer->char r))
      (integer->char r)
      (random-alphanum-char))))

(define (generate-id)
  "Generate our own peer id.  A peer id is a random 20-bytes string.  Usually,
it is made readable in this format: -<two-letter program id><four-digits version>-<12 alphanumerical characters>."
  (string-append "-GT0001-"
                 (list->string
                   (let loop ((sz 0))
                     (if (< sz 12)
                       (cons (random-alphanum-char) (loop (+ 1 sz)))
                       '())))))

(define (merge-potential-peer old-peers new-peer)
  (cons
    (make-potential-peer (assoc-ref new-peer "host") (assoc-ref new-peer "port") 1)
    (filter (lambda (peer)
              (not (equal? (potential-peer-host peer) (assoc-ref new-peer "host"))))
            old-peers)))

(define (merge-potential-peers old-peers new-peers)
  (let ((peers (map (lambda (peer)
                      (make-potential-peer
                        (potential-peer-host peer)
                        (potential-peer-port peer)
                        (+ (potential-peer-freshness peer) 1)))
                    old-peers)))
    (filter
      (lambda (peer)
        (< (potential-peer-freshness peer) 5))
      (let loop ((peers peers) (new-peers new-peers))
        (if (null? new-peers)
          peers
          (loop (merge-potential-peer peers (car new-peers)) (cdr new-peers)))))))
