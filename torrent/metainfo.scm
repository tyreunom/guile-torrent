;;;; Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (torrent metainfo)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9)
  #:use-module (torrent bencoding)
  #:use-module (torrent utils sha-1)
  #:export (parse-torrent-file
            make-metainfo
            metainfo?
            metainfo-info
            metainfo-announce
            metainfo-infohash
            
            make-torrent-info
            torrent-info?
            torrent-info-piece-length
            torrent-info-pieces
            torrent-info-private?
            torrent-info-single-file?
            torrent-info-name
            torrent-info-len
            torrent-info-files
            
            make-torrent-file
            torrent-file?
            torrent-file-len
            torrent-file-path))

(define-record-type metainfo
  (make-metainfo info announce infohash)
  metainfo?
  (info     metainfo-info)
  (announce metainfo-announce)
  (infohash metainfo-infohash))

(define-record-type torrent-info
  (make-torrent-info piece-length pieces private? single-file? name len files)
  torrent-info?
  (piece-length torrent-info-piece-length)
  (pieces       torrent-info-pieces)
  (private?     torrent-info-private?)
  (single-file? torrent-info-single-file?)
  (name         torrent-info-name)
  (len          torrent-info-len)
  (files        torrent-info-files))

(define-record-type torrent-file
  (make-torrent-file len path)
  torrent-file?
  (len  torrent-file-len)
  (path torrent-file-path))

(define (parse-file file)
  (let* ((file (dictionnary-alist file)))
    (make-torrent-file
      (assoc-ref file "len")
      (map (lambda (p) (utf8->string p))(assoc-ref file "path")))))

(define (split-pieces pieces)
  (let loop ((offset 0) (result '()))
    (if (>= offset (bytevector-length pieces))
      result
      (begin
        (let ((bv (make-bytevector 20 0)))
          (bytevector-copy! pieces offset bv 0 20)
          (loop (+ offset 20) (cons bv result)))))))

(define (parse-info info)
  (let* ((info (dictionnary-alist info))
         (single-file? (not (assoc-ref info "files"))))
    (make-torrent-info
      (assoc-ref info "piece length")
      (reverse (split-pieces (assoc-ref info "pieces")))
      (assoc-ref info "private")
      single-file?
      (assoc-ref info "name")
      (assoc-ref info "length")
      (if single-file? #f
        (map parse-file (assoc-ref info "files"))))))

(define (parse-torrent-file file)
  (call-with-input-file file
    (lambda (port)
      (let ((content (dictionnary-alist (bdecode port))))
        (make-metainfo
          (parse-info (assoc-ref content "info"))
          (utf8->string (assoc-ref content "announce"))
          (sha-1->bytevector (sha-1 (bencode (assoc-ref content "info")))))))))
