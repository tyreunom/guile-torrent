;;;; Copyright (C) 2018, 2019 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (torrent protocol)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 match)
  #:use-module (rnrs base)
  #:use-module (rnrs bytevectors)
  #:use-module (torrent utils fibers)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:export (send-handshake
            receive-handshake
            receive-message
            send-keep-alive
            send-choke
            send-unchoke
            send-interested
            send-not-interested
            send-have
            send-bitfield
            send-request
            send-piece
            send-cancel
            send-port
	    has-piece?
	    has-piece))

(define (send-handshake sock infohash self-id)
  (let ((handshake (make-bytevector 68)))
    (bytevector-u8-set! handshake 0 19)
    (bytevector-copy! (string->utf8 "BitTorrent protocol") 0 handshake 1 19)
    (bytevector-copy! infohash 0 handshake 28 20)
    (bytevector-copy! self-id 0 handshake 48 20)
    (put-bytevector sock handshake)))

(define (receive-handshake sock infohash)
  (let* ((handshake (get-bytevector-n sock 68))
         (proto (make-bytevector 19))
         (rinfohash (make-bytevector 20))
         (rpeerid (make-bytevector 20)))
    (bytevector-copy! handshake 1 proto 0 19)
    (bytevector-copy! handshake 28 rinfohash 0 20)
    (bytevector-copy! handshake 48 rpeerid 0 20)
    (if (and (equal? (bytevector-u8-ref handshake 0) 19)
             (equal? (utf8->string proto) "BitTorrent protocol")
             (equal? infohash rinfohash))
      rpeerid
      (begin
        (close sock)
        #f))))

(define (receive-message sock)
  (let* ((len (bytevector-u32-ref (get-bytevector-n sock 4) 0 (endianness big)))
         (msg (if (eq? len 0) #f (get-bytevector-n sock len))))
    (if (eq? len 0)
      'keepalive
      (begin
        (if (eq? (bytevector-length msg) len)
          (let ((id (bytevector-u8-ref msg 0)))
            (match id
              (0 'choke)
              (1 'unchoke)
              (2 'interested)
              (3 'not-interested)
              (4 (let ((piece (bytevector-u32-ref msg 1 (endianness big))))
                   `(("type" . have) ("piece" . ,piece))))
              (5 (let ((bitfield (make-bytevector (- len 1) 0)))
                   (bytevector-copy! msg 1 bitfield 0 (- len 1))
                   `(("type" . bitfield) ("bitfield" . ,bitfield))))
              (6 (let ((index (bytevector-u32-ref msg 1 (endianness big)))
                       (start (bytevector-u32-ref msg 5 (endianness big)))
                       (len (bytevector-u32-ref msg 9 (endianness big))))
                   `(("type" . request)
                     ("index" . ,index)
                     ("start" . ,start)
                     ("len" . ,len))))
              (7 (let ((index (bytevector-u32-ref msg 1 (endianness big)))
                       (start (bytevector-u32-ref msg 5 (endianness big)))
                       (block (make-bytevector (- len 9))))
                   (bytevector-copy! msg 9 block 0 (- len 9))
                   `(("type" . piece)
                     ("index" . ,index)
                     ("start" . ,start)
                     ("block" . ,block))))
              (8 (let ((index (bytevector-u32-ref msg 1 (endianness big)))
                       (start (bytevector-u32-ref msg 5 (endianness big)))
                       (len (bytevector-u32-ref msg 9 (endianness big))))
                   `(("type" . cancel)
                     ("index" . ,index)
                     ("start" . ,start)
                     ("len" . ,len))))
              (9 (let ((port (bytevector-u16-ref msg 1 (endianness big))))
                   `(("type" . port) ("port" . ,port))))))
          #f)))))

(define (send-keep-alive sock)
  (let ((msg (u8-list->bytevector '(0))))
    (put-bytevector sock msg)))

(define (send-choke sock)
  (let ((msg (u8-list->bytevector '(0 0 0 1 1))))
    (put-bytevector sock msg)))

(define (send-unchoke sock)
  (let ((msg (u8-list->bytevector '(0 0 0 1 2))))
    (put-bytevector sock msg)))

(define (send-interested sock)
  (let ((msg (u8-list->bytevector '(0 0 0 1 3))))
    (put-bytevector sock msg)))

(define (send-not-interested sock)
  (let ((msg (u8-list->bytevector '(0 0 0 1 4))))
    (put-bytevector sock msg)))

(define (send-have sock piece)
  (let ((msg (u8-list->bytevector '(0 0 0 0 5 5 0 0 0 0))))
    (bytevector-u32-set! msg 6 piece (endianness big))
    (put-bytevector sock msg)))

(define (send-bitfield sock bitfield)
  (let ((msg (make-bytevector (+ (bytevector-length bitfield) 5))))
    (bytevector-u32-set! msg 0 (+ (bytevector-length bitfield) 1) (endianness big))
    (bytevector-u8-set! msg 4 5)
    (bytevector-copy! bitfield 0 msg 6 (bytevector-length bitfield))
    (put-bytevector sock msg)))

(define (send-request sock index start len)
  (let ((msg (make-bytevector 17)))
    (bytevector-u32-set! msg 0 13 (endianness big))
    (bytevector-u8-set! msg 4 6)
    (bytevector-u32-set! msg 5 index (endianness big))
    (bytevector-u32-set! msg 9 start (endianness big))
    (bytevector-u32-set! msg 13 len (endianness big))
    (put-bytevector sock msg)))

(define (send-piece sock index start block)
  (let ((msg (make-bytevector (+ (bytevector-length block) 13))))
    (bytevector-u32-set! msg 0 (+ (bytevector-length block) 9) (endianness big))
    (bytevector-u8-set! msg 4 7)
    (bytevector-u32-set! msg 5 index (endianness big))
    (bytevector-u32-set! msg 9 start (endianness big))
    (bytevector-copy! block 0 msg 13 (bytevector-length block))
    (put-bytevector sock msg)))

(define (send-cancel sock index start len)
  (let ((msg (make-bytevector 17)))
    (bytevector-u32-set! msg 0 13 (endianness big))
    (bytevector-u8-set! msg 4 8)
    (bytevector-u32-set! msg 5 index (endianness big))
    (bytevector-u32-set! msg 9 start (endianness big))
    (bytevector-u32-set! msg 13 len (endianness big))
    (put-bytevector sock msg)))

(define (send-port sock listen-port)
  (let ((msg (u8-list->bytevector `(0 0 0 3 9 0 0))))
    (bytevector-u16-set! msg 5 listen-port (endianness big))
    (put-bytevector sock msg)))

(define (n-th-bit n num)
  (if (eq? n 7) (mod num 2) (n-th-bit (+ n 1) (div num 2))))

(define (has-piece? bitfield piece)
  "Returns whether @var{bitfield}, a bytevector representing a bitfield has bit
@var{piece} set."
  (n-th-bit (mod piece 8) (bytevector-u8-ref bitfield (div piece 8))))

(define (update-bit n num)
  (if (eq? n 7)
    (+ 1 (* 2 (div num 2)))
    (+ (mod num 2) (* 2 (update-bit (+ n 1) (div num 2))))))

(define (has-piece bitfield piece)
  "Set bit @var{piece} of @var{bitfield}, a bytevector representing a bitfield."
  (bytevector-u8-set!
    bitfield (div piece 8)
    (update-bit (mod piece 8) (bytevector-u8-ref bitfield (div piece 8)))))
