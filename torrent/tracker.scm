;;;; Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (torrent tracker)
  #:use-module (fibers channels)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-9)
  #:use-module (torrent bencoding)
  #:use-module (torrent utils fibers)
  #:use-module (web request)
  #:use-module (web response)
  #:use-module (web uri)
  #:export (make-tracker
            tracker?
            tracker-host
            tracker-page
            tracker-proto
            tracker-port
            tracker-infohash
            tracker-state
            tracker-uploaded
            tracker-downloaded
            tracker-left
            tracker-id

            make-tracker-response
            tracker-response?
            tracker-response-failure-reason
            tracker-response-warning-message
            tracker-response-interval
            tracker-response-min-interval
            tracker-response-tracker-id
            tracker-response-complete
            tracker-response-incomplete
            tracker-response-peers

            uri-proto
            uri-port
            uri-host
            uri-page

            url-encode
            get-new-tracker
            ask-more-peers
            
            tracker-fiber))

(define-record-type tracker
  (make-tracker host page proto port infohash state uploaded downloaded left id)
  tracker?
  (host       tracker-host)
  (page       tracker-page)
  (proto      tracker-proto)
  (port       tracker-port)
  (infohash   tracker-infohash)
  (state      tracker-state)
  (uploaded   tracker-uploaded)
  (downloaded tracker-downloaded)
  (left       tracker-left)
  (id         tracker-id))

(define-record-type tracker-response
  (make-tracker-response failure-reason warning-message interval min-interval
                         tracker-id complete incomplete peers)
  tracker-response?
  (failure-reason  tracker-response-failure-reason)
  (warning-message tracker-response-warning-message)
  (interval        tracker-response-interval)
  (min-interval    tracker-response-min-interval)
  (tracker-id      tracker-response-tracker-id)
  (complete        tracker-response-complete)
  (incomplete      tracker-response-incomplete)
  (peers           tracker-response-peers))

(define (uri-proto uri)
  (car (string-split uri #\:)))

(define (uri-host uri)
  (car (string-split
         (substring (car (cdr (string-split uri #\:))) 2)
         #\/)))

(define (uri-page uri)
  (string-append
    "/"
    (string-join
      (cdr (string-split
             (substring (string-concatenate (cdr (string-split uri #\:))) 2)
             #\/))
      "/")))

(define (uri-port uri)
  (match (cdr (string-split
                (car
                  (string-split
                    (substring (string-join (cdr (string-split uri #\:)) ":") 2)
                    #\/)) #\:))
    ('() #f)
    ((port rest ...) (string->number port))))

(define (get-new-tracker uri infohash left)
  (make-tracker (uri-host uri) (uri-page uri) (uri-proto uri) (uri-port uri)
                infohash 'start 0 0 left #f))

(define (url-encode bv)
  (string-concatenate
    (map
      (lambda (int) (with-output-to-string (lambda _ (format #t "%~2,'0X" int))))
      (bytevector->u8-list bv))))

(define (parse-peers peers)
  (match peers
    (#f '())
    ((? bytevector? peers)
     (begin
       (let loop ((pos 0))
         (if (< pos (bytevector-length peers))
           (cons `(("host" . ,(with-output-to-string
                                (lambda _
                                  (format #t "~d.~d.~d.~d"
                                          (bytevector-u8-ref peers pos)
                                          (bytevector-u8-ref peers (+ pos 1))
                                          (bytevector-u8-ref peers (+ pos 2))
                                          (bytevector-u8-ref peers (+ pos 3))))))
                   ("port" . ,(bytevector-u16-ref peers (+ pos 4) (endianness big))))
                 (loop (+ pos 6)))
           '()))))))

(define (parse-tracker-response response)
  (let ((response (dictionnary-alist response)))
    (make-tracker-response
      (assoc-ref response "failure reason")
      (assoc-ref response "warning message")
      (assoc-ref response "interval")
      (assoc-ref response "min-interval")
      (assoc-ref response "tracker id")
      (assoc-ref response "complete")
      (assoc-ref response "incomplete")
      (parse-peers (assoc-ref response "peers")))))

(define (ask-more-peers tracker peerid port)
  (let* ((request (string-append
                    "compact=1&"
                    "info_hash=" (url-encode (tracker-infohash tracker)) "&"
                    "peer_id=" (url-encode peerid) "&"
                    "port=" (number->string port) "&"
                    "uploaded=" (number->string (tracker-uploaded tracker)) "&"
                    "downloaded=" (number->string (tracker-downloaded tracker)) "&"
                    "left=" (number->string (tracker-left tracker)) "&"
                    (match (tracker-state tracker)
                      ('start "event=started")
                      ('complete "event=completed")
                      ('stop "event=stopped")
                      (_ ""))
                    (if (tracker-id tracker)
                      (string-append "&trackerid=" (tracker-id tracker))
                      "")))
         (uri (build-uri
                (match (tracker-proto tracker)
                  ("http" 'http)
                  ("https" 'https))
                #:port (tracker-port tracker)
                #:host (tracker-host tracker)
                #:path (tracker-page tracker)
                #:query request))
         (port (tracker-port tracker))
         (info (car (getaddrinfo (tracker-host tracker)
                                 (if port
                                   (number->string port)
                                   (tracker-proto tracker))
                                 (if port
                                   AI_NUMERICSERV
                                   0))))
         (sock (socket (addrinfo:fam info) SOCK_STREAM IPPROTO_IP)))
    (connect sock (addrinfo:addr info))
    (write-request (build-request uri) sock)
    (let* ((response (read-response sock))
           (ans (bdecode (response-port response))))
      (close-port sock)
      (if (eq? (response-code response) 200)
        (parse-tracker-response ans)
        (list uri response)))))

(define (tracker-fiber tracker peerid port infohash ending-channel peers-channel)
  (lambda ()
    (let ((tracker (get-new-tracker tracker infohash 1000)))
      (let loop ((tracker tracker))
        (let* ((response (ask-more-peers tracker peerid port))
               (tracker
                 (make-tracker
                   (tracker-host tracker)
                   (tracker-page tracker)
                   (tracker-proto tracker)
                   (tracker-port tracker)
                   (tracker-infohash tracker)
                   (tracker-state tracker)
                   (tracker-uploaded tracker)
                   (tracker-downloaded tracker)
                   (tracker-left tracker)
                   (if (tracker-response-tracker-id response)
                     (tracker-response-tracker-id response)
                     (tracker-id tracker))))
               (timer (if (tracker-response-interval response)
                       (tracker-response-interval response)
                       300)))
          (if (not (tracker-response-failure-reason response))
            (put-message
              peers-channel
              (list
                (cons "type" 'new-peers)
                (cons "new-peers" (tracker-response-peers response))))
            (format #t "Error while connecting to tracker: ~a~%"
                    (utf8->string (tracker-response-failure-reason response))))
          (if (tracker-response-warning-message response)
            (format #t "Warning: ~a~%" (tracker-response-warning-message response)))
          (let ((timeout-channel (make-channel)))
            (match
              (timeout-perform-operation
                timer
                (get-operation ending-channel))
              ('stop #t)
              ('timeout (loop tracker)))))))))
