;;;; Copyright (C) 2018, 2019 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (torrent utils fibers)
  #:use-module (ice-9 match)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (fibers timers)
  #:export (timeout-perform-operation put-nonblocking commanded-fiber))

(define (timeout-perform-operation time op)
  (catch
    #t
    (lambda ()
      (match
        (perform-operation
          (choice-operation
            (sleep-operation time)
            op))
        ('timeout 'timeout)
        (val val)))
    (lambda _
      'timeout)))

(define (put-nonblocking channel message)
  (spawn-fiber
    (lambda ()
      (put-message channel message))))

(define-syntax commanded-fiber
  (syntax-rules ()
    ((_ handler (channel ...) (type => thunk) ...)
     (lambda ()
       (catch #t
	 (lambda ()
           (let loop ()
             (let ((message (timeout-perform-operation 60 (choice-operation (get-operation channel) ...))))
               (if (list? message)
                   (match (assoc-ref message "type")
                     ('quit #t)
                     (type
                       (begin
                         (apply thunk
                                (apply append
                                  (map
                                    (lambda (v)
                                      (list
                                        (symbol->keyword (string->symbol (car v)))
                                        (cdr v)))
                                    message)))
                         (loop)))
                     ...)))))
	 (lambda () (handler)))))))
