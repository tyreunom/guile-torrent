;;;; Copyright (C) 2018 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (torrent bencoding)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 binary-ports)
  #:use-module (rnrs bytevectors)
  #:export (make-dictionnary dictionnary-alist bencode bdecode))

(define-record-type dictionnary
  (make-dictionnary alist)
  dictionnary?
  (alist dictionnary-alist))

(define (bencode data)
  (match data
    ((? bytevector? dat)
     (let* ((sz (number->string (bytevector-length dat)))
            (bv (make-bytevector (+ (string-length sz) 1 (bytevector-length dat)))))
       (bytevector-copy! (string->utf8 sz) 0 bv 0 (string-length sz))
       (bytevector-u8-set! bv (string-length sz) (char->integer #\:))
       (bytevector-copy! dat 0 bv (+ 1 (string-length sz)) (bytevector-length dat))
       bv))
    ((? string? dat) (bencode (string->utf8 dat)))
    ((? number? dat)
     (string->utf8 (string-append "i" (number->string dat) "e")))
    ('() (string->utf8 "le"))
    ((? list? l)
     (let* ((bvs (map bencode l))
            (bv (make-bytevector (+ (fold (lambda (a b) (+ (bytevector-length a) b)) 0 bvs) 2))))
       (bytevector-u8-set! bv 0 (char->integer #\l))
       (let loop ((pos 1) (bvs bvs))
         (if (not (eq? bvs '()))
           (begin
             (let ((mbv (car bvs)))
               (bytevector-copy! mbv 0 bv pos (bytevector-length mbv))
               (loop (+ pos (bytevector-length mbv)) (cdr bvs))))))
       (bytevector-u8-set! bv (- (bytevector-length bv) 1) (char->integer #\e))
       bv))
    ((? dictionnary? d)
     (let* ((bvs (concatenate (map (lambda (entry) (match entry ((key . val) (list (bencode key) (bencode val))))) (dictionnary-alist d))))
            (bv (make-bytevector (+ (fold (lambda (a b) (+ (bytevector-length a) b)) 0 bvs) 2))))
       (bytevector-u8-set! bv 0 (char->integer #\d))
       (let loop ((pos 1) (bvs bvs))
         (if (not (eq? bvs '()))
           (begin
             (let ((mbv (car bvs)))
               (bytevector-copy! mbv 0 bv pos (bytevector-length mbv))
               (loop (+ pos (bytevector-length mbv)) (cdr bvs))))))
       (bytevector-u8-set! bv (- (bytevector-length bv) 1) (char->integer #\e))
       bv))))

(define (bdecode data)
  (bdecode-value data))

(define (bdecode-value data)
  (let ((c (get-u8 data)))
    (match (integer->char c)
      (#\e #f)
      (#\i (bdecode-number data))
      (#\l (bdecode-list data))
      (#\d (bdecode-dictionnary data))
      (chr (if (char-set-contains? (char-set-union (char-set #\-) char-set:digit) chr)
           (begin
             (unget-bytevector data (make-bytevector 1 c))
             (bdecode-string data)))))))

(define (bdecode-number data)
  (string->number (read-delimited "e:" data)))

(define (bdecode-list data)
  (let ((val (bdecode-value data)))
    (if (eq? val #f)
      '()
      (cons val (bdecode-list data)))))

(define (bdecode-dictionnary data)
  (let ((key (bdecode-value data)))
    (if (eq? key #f)
      (make-dictionnary '())
      (let ((val (bdecode-value data)))
        (make-dictionnary (cons `(,(utf8->string key) . ,val)
                                (dictionnary-alist (bdecode-dictionnary data))))))))

(define (bdecode-string data)
  (let ((sz (bdecode-number data)))
    (get-bytevector-n data sz)))
